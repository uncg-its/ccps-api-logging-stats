<?php

namespace Uncgits\CcpsApiLog;

use Illuminate\Database\Eloquent\Model;

class ApiCallStatistic extends Model
{
    protected $guarded = [];
}
