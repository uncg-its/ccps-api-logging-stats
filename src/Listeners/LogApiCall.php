<?php

namespace Uncgits\CcpsApiLog\Listeners;

use Uncgits\CcpsApiLog\ApiCallStatistic;
use Uncgits\CcpsApiLog\Events\ApiCallAttempted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogApiCall
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiCallAttempted  $event
     * @return void
     */
    public function handle(ApiCallAttempted $event)
    {
        ApiCallStatistic::create([
            'service' => $event->service,
            'method' => $event->result['response']['method'],
            'endpoint' => $event->result['response']['endpoint'],
            'result_code' => $event->result['response']['httpCode'],
            'result_reason' => $event->result['response']['httpReason']
        ]);
    }
}
