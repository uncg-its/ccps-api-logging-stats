<?php

namespace Uncgits\CcpsApiLog;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Uncgits\CcpsApiLog\Events\ApiCallAttempted;
use Uncgits\CcpsApiLog\Listeners\LogApiCall;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'ccps-api-log');

        $this->loadMigrationsFrom(__DIR__ . '/migrations');

        $this->loadRoutesFrom(__DIR__ . '/routes/ccps-api-log.php');

        Event::listen(ApiCallAttempted::class, LogApiCall::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}