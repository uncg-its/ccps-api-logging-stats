@extends('layouts.wrapper', [
    'pageTitle' => 'Live | API Statistics'
])

@section('content')
    {!! Breadcrumbs::render('live-statistics') !!}
    <h2>API Statistics</h2>
    <p>Showing API call statistics for <strong>past {{ $number }} {{ str_plural($mode, $number) }}</strong></p>

    <div class="card mb-3">
        <div class="card-body bg-light">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Change Date Range:</strong></p>
                    <p>
                        <a href="/live/statistics?hours=1" class="btn btn-sm btn-primary">1 hour</a>
                        <a href="/live/statistics?hours=12" class="btn btn-sm btn-primary">12 hours</a>
                    </p>
                    <p>
                        <a href="/live/statistics?days=1" class="btn btn-sm btn-primary">1 day</a>
                        <a href="/live/statistics?days=7" class="btn btn-sm btn-primary">7 days</a>
                        <a href="/live/statistics?days=30" class="btn btn-sm btn-primary">30 days</a>
                        <a href="/live/statistics?days=60" class="btn btn-sm btn-primary">60 days</a>
                        <a href="/live/statistics?days=90" class="btn btn-sm btn-primary">90 days</a>
                    </p>
                </div>
                <div class="col-md-6">
                    <p>Custom</p>
                    <form action="/live/statistics" method="get" class="form-inline">
                        <div class="form-group">
                            <label for="hours"># of Hours:</label>
                            <input type="text" class="form-control-sm mx-3" id="hours" name="hours" placeholder="# of Hours">
                        </div>

                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Go</button>
                    </form>
                    <br>
                    <form action="/live/statistics" method="get" class="form-inline">
                        <div class="form-group">
                            <label for="days"># of Days:</label>
                            <input type="text" class="form-control-sm mx-3" id="days" name="days" placeholder="# of Days">
                        </div>

                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Go</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <h4>Total API Calls: {{ count($calls) }}</h4>

    <hr>
    <h4>Results by:</h4>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a href="#responseCode" class="nav-link active" aria-controls="responseCode" role="tab" data-toggle="tab">Response Code</a>
        </li>
        <li class="nav-item">
            <a href="#endpoint" class="nav-link" aria-controls="endpoint" role="tab" data-toggle="tab">Endpoint</a>
        </li>
        <li class="nav-item">
            <a href="#method" class="nav-link" aria-controls="method" role="tab" data-toggle="tab">Method</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="responseCode">
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>Response Code</th>
                    <th>Number of Calls</th>
                </tr>
                </thead>
                <tbody>
                @foreach($calls->groupBy('result_code') as $codeGroup => $codeCalls)
                    <tr>
                        <td>{{ $codeGroup }}</td>
                        <td>{{ count($codeCalls) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <hr>
            <div id="responseCodeChart"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="endpoint">
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>Service</th>
                    <th>Number of Calls</th>
                </tr>
                </thead>
                <tbody>
                @foreach($calls->groupBy('endpoint') as $codeGroup => $codeCalls)
                    <tr>
                        <td>{{ $codeGroup }}</td>
                        <td>{{ count($codeCalls) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <hr>
            <div id="endpointChart"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="method">
            <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>Method</th>
                    <th>Number of Calls</th>
                </tr>
                </thead>
                <tbody>
                @foreach($calls->groupBy('method') as $codeGroup => $codeCalls)
                    <tr>
                        <td>{{ $codeGroup }}</td>
                        <td>{{ count($codeCalls) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <hr>
            <div id="methodChart"></div>
        </div>
    </div>

@endsection()

@section('scripts')
    {!! $responseCodeChart->printScripts() !!}
    <script>
        {!! $responseCodeChart->render('responseCodeChart') !!}
        {!! $endpointChart->render('endpointChart') !!}
        {!! $methodChart->render('methodChart') !!}
    </script>
@endsection