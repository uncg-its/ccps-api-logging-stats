<?php

namespace Uncgits\CcpsApiLog\Http\Controllers;

use Uncgits\CcpsApiLog\ApiCallStatistic;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Ghunti\HighchartsPHP\Highchart;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function index(Request $request) {
        // hours or days?
        if (!is_null($request->hours)) {
            $mode = 'hour';
            $number = (!is_numeric($request->hours)) ? 24 : $request->hours;
        } else {
            $mode = 'day';
            $number = (!is_numeric($request->days)) ? 7 : $request->days;
        }


        $calls = ApiCallStatistic::where('created_at', '>=', Carbon::parse("$number {$mode}s ago"))->get();

        // response code chart
        $responseCodeChart = new Highchart();
        $responseCodeChart->chart = [
            'renderTo' => 'responseCodeChart',
            'type' => 'bar'
        ];

        $responseCodeChart->legend->enabled = false;

        $responseCodeChart->title->text = 'Responses by Code';
        $responseCodeChart->xAxis = [
            'title' => [
                'text' => 'Response Code'
            ]
        ];

        $responseCodeChart->yAxis = [
            'min' => 0,
            'title' => [
                'text' => 'Calls'
            ]
        ];

        $responseCodeChart->series[0]->name = 'Calls';

        foreach($calls->groupBy('result_code') as $group => $callsData) {
            $responseCodeChart->series[0]->data[] = count($callsData);
            $responseCodeChart->xAxis->categories[] = $group;
        }


        // endpoint chart

        $endpointChart = new Highchart();
        $endpointChart->chart = [
            'renderTo' => 'endpointChart',
            'type' => 'pie'
        ];

        $endpointChart->plotOptions->pie->dataLabels->format = '{point.name} (<b>{point.percentage:.1f}%</b>)';

        $endpointChart->title->text = 'Calls per Endpoint';
        $endpointChart->xAxis = [
            'categories' => [],
            'title' => [
                'text' => 'Endpoint'
            ]
        ];

        $endpointChart->yAxis = [
            'min' => 0,
            'title' => [
                'text' => 'Calls'
            ]
        ];

        $endpointChart->series[0]->name = 'Calls';
        $endpointChart->series[0]->colorByPoint = true;

        foreach($calls->groupBy('endpoint') as $group => $callsData) {
            $endpointChart->series[0]->data[] = [
                'name' => $group,
                'y' => count($callsData)
            ];
        }

        // method chart
        $methodChart = new Highchart();
        $methodChart->chart = [
            'renderTo' => 'methodChart',
            'type' => 'bar'
        ];

        $methodChart->legend->enabled = false;

        $methodChart->title->text = 'Responses by Method';
        $methodChart->xAxis = [
            'title' => [
                'text' => 'Response Method'
            ]
        ];

        $methodChart->yAxis = [
            'min' => 0,
            'title' => [
                'text' => 'Calls'
            ]
        ];

        $methodChart->series[0]->name = 'Calls';

        foreach($calls->groupBy('method') as $group => $callsData) {
            $methodChart->series[0]->data[] = count($callsData);
            $methodChart->xAxis->categories[] = $group;
        }



        return view('ccps-api-log::statistics.index')->with([
            'mode' => $mode,
            'number' => $number,
            'calls' => $calls,
            'responseCodeChart' => $responseCodeChart,
            'endpointChart' => $endpointChart,
            'methodChart' => $methodChart
        ]);
    }
}
