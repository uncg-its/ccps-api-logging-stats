# UNCG CCPS Core - API Logging Statistics

This package is an add-on for apps built with [CCPS Core](https://bitbucket.org/uncg-its/ccps-core) that is designed to provide logging and statistics around the number of API calls made by the application.

## This package provides:
- a view and controller
- a database migration
- an Event / Listener pair

## Who maintains this?

The UNCG CCPS Developers Group maintains this app - ccps-developers-l@uncg.edu

# Installation

## 1. Require the package

```php
composer require uncgits/ccps-api-logging-stats
```

## 2. Register the service provider

Since CCPS Core requires a version of Laravel that includes package auto-discovery, registration of the Service Provider is not necessary.

## 3. Run the migration

```php
php artisan migrate
```

## 4. Add route

In your `routes/web.php` file, add the statistics page route into your app in your own routes file (pointing to `Uncgits\CcpsApiLog\Http\Controllers\StatisticsController@index`). Something like:

```php
Route::get('apistats', 'Uncgits\CcpsApiLog\Http\Controllers\StatisticsController@index')
```

# Usage

There is no need to register the event+listener combination in your `EventServiceProvider.php` file - this is handled in this package's `ServiceProvider.php` file.

## Firing an event

All you need to do when using this package is to fire the event after you attempt an API call. The event expects two parameters - the first parameter is the _resultset from the call_, and the second is the _service name_.

So you might do something like this:

```php
// make API call however you do in your package.
$resultset = $api->makeCall('param', 'param', 'etc');
event(new ApiCallAttempted($resultset, 'my-service-name'));
```

The _resultset_ is an array that should contain at least:

* the method (GET, POST, etc.),
* the endpoint (can be whatever you want - full URI, or a custom string that indicates which API method was hit, etc.)
* the result code (HTTP code, like 200, 403, etc.)
* the result reason ("OK", "Forbidden", etc.)

In the Listener, these values are sought out and passed to the `ApiCallStatistic` model for insertion into the database:

```php
'service' => $event->service,
'method' => $event->result['response']['method'],
'endpoint' => $event->result['response']['endpoint'],
'result_code' => $event->result['response']['httpCode'],
'result_reason' => $event->result['response']['httpReason']
```

In the future customization of these keys may be possible, but at present they are hard-coded.